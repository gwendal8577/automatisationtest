describe("First simple page", () => {
	it("Should have a title", () => {
		cy.visit("/");
		cy.get("h1")
			.should("have.text", "10 Richest People");
	});
});