describe("First simple page", () => {
	it("Should have a button for check order", () => {
		cy.visit("/");
		cy.get("body")
			.contains('Check Order')
			.should('have.id', 'check')
			.should('not.be.disabled')
		cy.get('li')
            .should('have.length', 10)
		cy.wait(7000)
		cy.get('#check')
			.click()
		cy.get('ul>li').eq(0)
			.should('contain', 'Jeff Bezos')
			.should('have.class', 'right')
	});
});