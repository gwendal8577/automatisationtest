var assert = require('assert');
var should = require('should');

var tableau = ['Jeff Bezos',
'Bill Gates',
'Warren Buffett',
'Bernard Arnault',
'Carlos Slim Helu',
'Amancio Ortega',
'Larry Ellison',
'Mark Zuckerberg',
'Michael Bloomberg',
'Larry Page'];

describe('unit test', function() {
  describe('#getRichest()', function() {
    it('find Jeff Bezos in the array at the position of 1', function() {
        tableau.indexOf('Jeff Bezos').should.equal(0);
    });
  });
  describe('#getRichest()', function() {
    it('find Bill Gates in the array at the position of 2', function() {
        tableau.indexOf('Bill Gates').should.equal(1);
    });
  });
  describe('#getRichest()', function() {
    it('find Warren Buffett in the array at the position of 3', function() {
        tableau.indexOf('Warren Buffett').should.equal(2);
    });
  });
  describe('#getRichest()', function() {
    it('find Bernard Arnault in the array at the position of 4', function() {
        tableau.indexOf('Bernard Arnault').should.equal(3);
    });
  });
  describe('#getRichest()', function() {
    it('find Carlos Slim Helu in the array at the position of 5', function() {
        tableau.indexOf('Carlos Slim Helu').should.equal(4);
    });
  });
  describe('#getRichest()', function() {
    it('find Amancio Ortega in the array at the position of 6', function() {
        tableau.indexOf('Amancio Ortega').should.equal(5);
    });
  });
  describe('#getRichest()', function() {
    it('find Larry Ellison in the array at the position of 7', function() {
        tableau.indexOf('Larry Ellison').should.equal(6);
    });
  });
  describe('#getRichest()', function() {
    it('find Mark Zuckerberg in the array at the position of 8', function() {
        tableau.indexOf('Mark Zuckerberg').should.equal(7);
    });
  });
  describe('#getRichest()', function() {
    it('find Michael Bloomberg in the array at the position of 9', function() {
        tableau.indexOf('Michael Bloomberg').should.equal(8);
    });
  });
  describe('#getRichest()', function() {
    it('find Larry Page in the array at the position of 10', function() {
        tableau.indexOf('Larry Page').should.equal(9);
    });
  });
});

describe('unittest', function() {
  describe('#getNombreDePersonnes', function() {
    it('test si le nombre de personne est égale a 10', function() {
      assert.equal(tableau.length, 10);
    });
  });
});

